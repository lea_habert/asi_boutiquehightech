package BoutiqueHighTech.ressources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import BoutiqueHighTech.dao.Dao;
import BoutiqueHighTech.model.Categorie;

public class CategoriesRessources {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	Integer key;
	public CategoriesRessources(UriInfo uriInfo, Request request, Integer key){
		this.uriInfo = uriInfo;
        this.request = request;
        this.key = key;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_XML})
	public List<Categorie> getAllCategories() {
		List<Categorie> categories = new ArrayList<>();
		categories.addAll(Dao.getModelCategorie().values());
		return categories;
	}
	
	/*
	@GET
	@Produces({MediaType.APPLICATION_XML})
	public Categorie getCategorieById(Integer key) {
		Categorie categorie = Dao.getModelCategorie().get(key);
		return categorie;
	}
	
	@POST
	@Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void addCategorie(Integer key, Categorie categorie) {	// TODO Response 
		Dao.getModelCategorie().put(key, categorie);
	}
	
	@DELETE
	 public void removeCategorie(Integer key) {  // TODO Response 
		 Dao.getModelCategorie().remove(key);
	 }
	 
	 @GET
    @Produces({MediaType.APPLICATION_XML})
    public List<Article> getListArticle(Integer key) {
        List<Article> articleList = new ArrayList<>();
        articleList.addAll(Dao.getModelCategorie().get(key).getArticleList());
        return articleList;
    }
	*/
}
