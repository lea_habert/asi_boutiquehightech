package BoutiqueHighTech.ressources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;
import javax.xml.ws.Response;

import BoutiqueHighTech.dao.Dao;
import BoutiqueHighTech.model.Boutique;


public class BoutiqueRessources {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	Integer key;
	
	public BoutiqueRessources(UriInfo uriInfo, Request request, Integer key) {
		this.uriInfo = uriInfo;
	    this.request = request;
	    this.key = key;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<Boutique> getAllBoutique() {
		List<Boutique> boutiques = new ArrayList<>();
		boutiques.addAll(Dao.getModelBoutique().values());
		return boutiques;
	}
	
	/*
	@GET
	@Produces({MediaType.APPLICATION_XML})
	public Boutique getBoutiqueByKey() {
		Boutique boutique = Dao.getModelBoutique().get(key);
		return boutique;
	}
	
	@PUT
	@Path("boutique_update")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void updateBoutique(Integer keyBoutique, Integer keyAdmin,
			@FormParam("description") String description,
			@FormParam("adresse") String adresse,
			@FormParam("mail") String mail,
			@FormParam("telephone") String telephone) {
		// TODO 
		Boutique boutique = Dao.getModelBoutique().get(key);
		boutique.setDescription(description);
		boutique.setAdressse(adresse);
		boutique.setMail(mail);
		boutique.setTelephone(telephone);	
	}	
	*/
}
