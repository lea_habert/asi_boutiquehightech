package BoutiqueHighTech.ressources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import BoutiqueHighTech.dao.Dao;
import BoutiqueHighTech.model.Article;
import BoutiqueHighTech.model.Categorie;

@Path("/articles")
public class ArticlesRessources {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	Integer key;
	
	public ArticlesRessources(UriInfo uriInfo, Request request, Integer key){
		this.uriInfo = uriInfo;
        this.request = request;
        this.key = key;
	}
	
	@GET
	@Produces({MediaType.TEXT_XML})
	public List<Article> getAllArticles() {
		List<Article> articles = new ArrayList<>();
		articles.addAll(Dao.getModelArticle().values());
		return articles;
	}
	/*
	@GET
	@Produces({MediaType.APPLICATION_XML})
	public Article getArticleById(Integer key) {
		Article article = Dao.getModelArticle().get(key);
		return article;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_XML})
	public List<Article> getArticleByCategorie(Categorie categorie) {
		List<Article> articles = new ArrayList<>();
		for(int i = 0 ; i < Dao.getModelArticle().size(); i++)
			if(Dao.getModelArticle().get(null).getCategorie() == categorie) {
				articles.add(Dao.getModelArticle().get(null));
			}
		return articles;
	}
	
	@POST
	@Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void addArticle(Integer key, Article article) {	
		Dao.getModelArticle().put(key, article);
	}
	
	 @DELETE
	 public void removeArticle(Integer key) {  // TODO Response 
		 Dao.getModelArticle().remove(key);
	    }
	
	@PUT
	@Path("article_update")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void updateArticle(Integer key, 					
			@FormParam("libelle") String libelle,
			@FormParam("prix") Integer prix,
			@FormParam("categorie") Categorie categorie,
			@FormParam("oldCategorie") Categorie oldCategorie,
			@FormParam("photo") String photo) {
		
		Article article = Dao.getModelArticle().get(key);
		// on gere la modif d'un article en affichant sa premiere categorie
		article.replaceArticle(oldCategorie, categorie);
		article.setLibelle(libelle);
		article.setPrix(prix);
		article.setPhoto(photo);	
	}
	*/
}