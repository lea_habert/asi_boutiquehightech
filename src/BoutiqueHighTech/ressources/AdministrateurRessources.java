package BoutiqueHighTech.ressources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import BoutiqueHighTech.dao.Dao;
import BoutiqueHighTech.model.*;

public class AdministrateurRessources {
	 @Context
	 UriInfo uriInfo;
	 @Context
	 Request request;
	 Integer key;
	 
	
	 public AdministrateurRessources(UriInfo uriInfo, Request request, Integer key) {
		 this.uriInfo = uriInfo;
	     this.request = request;
	     this.key = key;
	 }
	 
	 @GET
	 @Produces({MediaType.APPLICATION_XML})
	 public List<Administrateur> getAllAdministrateur() {
		 List<Administrateur> admins = new ArrayList<Administrateur>();
		 admins.addAll(Dao.getModelAdministrateur().values());
		 return admins;
	 }
	 /*
	 
	 @GET
	 @Path("{admin}")
	 @Produces({MediaType.APPLICATION_XML})
	 public Administrateur getAdministrateurByKey(@PathParam("admin") Integer key) {
		 Administrateur admin = Dao.getModelAdministrateur().get(key);
		 return admin;
	 }
	 
	 //rajouter path
     @GET
     @Produces({MediaType.APPLICATION_XML})
     public List<Boutique> getBoutiqueAdministrateur(Integer key) {
         List<Boutique> boutiqueList = new ArrayList<Boutique>();
         boutiqueList.addAll(Dao.getModelAdministrateur().get(key).getBoutiqueAdmin());
         return boutiqueList;
     }
	 */
}
