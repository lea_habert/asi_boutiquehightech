package BoutiqueHighTech.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Administrateur {
	private String login;
	private String pwd;
	private String nom;
	private String prenom;
	private List<Boutique> boutiquesAdmin;
	
	//Constructeur
	public Administrateur(String login, String pwd, String nom, String prenom, Boutique boutique) {
		this.boutiquesAdmin = new ArrayList<>();
		this.login = login;
		this.pwd = pwd;
		this.nom = nom;
		this.prenom = prenom;
		this.boutiquesAdmin.add(boutique);
		
	}
	
	//Getters
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	
	public List<Boutique> getBoutiqueAdmin(){
		return boutiquesAdmin;
	}
	
	
}
