package BoutiqueHighTech.model;

import java.util.ArrayList;
import java.util.List;

public class Categorie {
	private int idCategorie;
	private String libelleCategorie;
	private List<Article> articleList;
	private List<Categorie> categorieList;
	
	//Constructeur
	public Categorie(int idCategorie, String libelleCategorie)
	{
		articleList = new ArrayList<>();
		categorieList = new ArrayList<>();
		this.idCategorie = idCategorie;
		this.libelleCategorie = libelleCategorie;
	}
	
	//Getters
	public List<Article> getArticleList(){
		return articleList;
	}
	
	public List<Categorie> getCategorieList() {
		return this.categorieList;
	}

	public int getIdCategorie() {
		return this.idCategorie;
	}
	
	public String getLibelleCategorie() {
		return this.libelleCategorie;
	}
	
	//Setters
	public void setLibelleCategorie(String libelleCategorie) {
		this.libelleCategorie = libelleCategorie;
	}
	
	public void addCategorieToList(Categorie categorie) {
		this.categorieList.add(categorie);
	}
	
	public void removeCategorieFromList(Categorie categorie) {
		this.categorieList.remove(categorie);
	}
	
	public void addArticle(Article article) {
		this.articleList.add(article);
		article.getCategorie().add(this);
	}
	
	public void removeArticle(Article article) {
		this.articleList.remove(article);
		article.getCategorie().remove(this);
	}
}
