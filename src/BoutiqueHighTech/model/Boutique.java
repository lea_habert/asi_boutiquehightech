package BoutiqueHighTech.model;

import java.util.ArrayList;
import java.util.List;

public class Boutique {
	private int idBoutique;
	private String description;
	private String adresse;
	private String mail;
	private String telephone;
	private List<Categorie> categorieList;
	
	//Constructeur
	public Boutique(int idBoutique, String description, String adresse, String mail, String telephone) {
		categorieList = new ArrayList<>();
		this.idBoutique = idBoutique;
		this.description = description;
		this.adresse = adresse;
		this.mail = mail;
		this.telephone = telephone;
	}
	
	//Getters Boutique
	
	public List<Categorie> getCategorieList(){
		return categorieList;
	}
	
	public int getIdBoutique() {
		return idBoutique;
	}
	public String getDescription() {
		return description;
	}
	public String getAdresse() {
		return adresse;
	}
	public String getMail() {
		return mail;
	}
	public String getTelephone() {
		return telephone;
	}
	
	//Setters Boutique
	public void setDescription (String description) {
		this.description = description;
	}
	public void setAdressse (String adresse) {
		this.adresse = adresse;
	}
	public void setMail (String mail) {
		this.mail = mail;
	}
	public void setTelephone (String telephone) {
		this.telephone = telephone;
	}
	
	// add
	public void addCategorie(Categorie categorie) {
		this.categorieList.add(categorie);
	}
	public void removeCategorie(Categorie categorie) {
		this.categorieList.remove(categorie);
	}
}
