package BoutiqueHighTech.dao;

import java.util.Map;

import BoutiqueHighTech.model.Administrateur;
import BoutiqueHighTech.model.Article;
import BoutiqueHighTech.model.Boutique;
import BoutiqueHighTech.model.Categorie;

public class Dao {
	private static Map<Integer, Administrateur> contentProviderAdministrateur;
	private static Map<Integer, Boutique> contentProviderBoutique; 
	private static Map<Integer, Article> contentProviderArticle;  
	private static Map<Integer, Categorie> contentProviderCategorie;
	
	private Dao() {
		Boutique b1 = new Boutique(1, "boutique tech", "11 rue de la paix", "tech@gmail.com", "06 15 15 08 15");
		contentProviderBoutique.put(1, b1);
		Administrateur adm1 = new Administrateur("admin", "admin", "Quellec", "Nathan", b1);
		contentProviderAdministrateur.put(1, adm1);
		Categorie c1 = new Categorie(1, "Informatique");
		contentProviderCategorie.put(1, c1);
		Article a1 = new Article(1, "ordinateur", 500.0, c1, "photo");
		contentProviderArticle.put(1, a1);
		
	}
	
	public static Map<Integer, Administrateur> getModelAdministrateur() {
		return contentProviderAdministrateur;
	}
	
	public static Map<Integer, Boutique> getModelBoutique() {
		return contentProviderBoutique;
	}
	
	public static Map<Integer, Article> getModelArticle() {
		return contentProviderArticle;
	}
	
	public static Map<Integer, Categorie> getModelCategorie() {
		return contentProviderCategorie;
	}
}
